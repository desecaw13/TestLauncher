package net.lil.testlauncher

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import net.lil.testlauncher.databinding.ActivityMainBinding

//todo:
// https://www.androidauthority.com/custom-launcher-part-two-838188/
// https://android.googlesource.com/platform/packages/apps/Launcher3/+/master
// https://github.com/LawnchairLauncher/Lawnchair
// https://github.com/Tbog/TBLauncher
// https://github.com/SimpleMobileTools/Simple-App-Launcher

private const val NUM_PAGES = 2

class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewPager = binding.viewPager

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = ScreenSlidePagerAdapter(this)
        viewPager.adapter = pagerAdapter
    }

    //fixme tmp
    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }

    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = NUM_PAGES

        override fun createFragment(position: Int): Fragment = when (position) {
            0 -> HomescreenFragment(applicationContext)
            1 -> AppsDrawerFragment()
            else -> throw IllegalStateException("The argument position must be between 0 and ${NUM_PAGES - 1}. It was $position.")
        }
    }
}
