package net.lil.testlauncher

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.preference.PreferenceFragmentCompat

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val backBtnKey = "back_button"

        requireActivity().applicationContext.getSharedPreferences(
            "net.lil.testlauncher_preferences",
            Context.MODE_PRIVATE
        ).registerOnSharedPreferenceChangeListener { sharedPrefs: SharedPreferences, s: String ->
            if (s != backBtnKey) return@registerOnSharedPreferenceChangeListener
            if (sharedPrefs.getBoolean(backBtnKey, false)) {
                val editor = sharedPrefs.edit()
                editor.putBoolean(backBtnKey, false)
                editor.apply()

                activity?.onBackPressed() ?: parentFragment?.activity?.onBackPressed()
                //fixme
            }
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }
}
