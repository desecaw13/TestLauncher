package net.lil.testlauncher

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.appwidget.AppWidgetHost
import android.appwidget.AppWidgetHostView
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProviderInfo
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import androidx.fragment.app.Fragment
import net.lil.testlauncher.databinding.FragmentHomescreenBinding

const val APPWIDGET_HOST_ID = R.id.APPWIDGET_HOST_ID
const val REQUEST_PICK_APPWIDGET = R.id.REQUEST_PICK_APPWIDGET
const val REQUEST_CREATE_APPWIDGET = R.id.REQUEST_CREATE_APPWIDGET

//todo:
// https://stackoverflow.com/questions/11473757/hosting-widgets-in-an-android-launcher
// https://developer.android.com/guide/topics/appwidgets/host
// move widget code to separate object

//todo: update code to use new Activity APIs
// https://dev.to/codewithmohit/startactivityforresult-is-deprecated-3hm3
// https://developer.android.com/training/basics/intents/result
// https://developer.android.com/reference/kotlin/androidx/activity/result/contract/ActivityResultContracts.StartActivityForResult

class HomescreenFragment(private val app_context: Context) : Fragment() {

    private val appWidgetManager = AppWidgetManager.getInstance(app_context)
    private val appWidgetHost = AppWidgetHost(app_context, APPWIDGET_HOST_ID)

    private lateinit var binding: FragmentHomescreenBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomescreenBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pum = PopupMenu(app_context, binding.settingsFab)

        pum.inflate(R.menu.settings_menu)

        pum.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.open_settings -> {
                    requireActivity().supportFragmentManager
                        .beginTransaction()
                        .replace(
                            ((view as ViewGroup).parent as View).id,
                            SettingsFragment()
                        )
                        .addToBackStack(null)
                        .commit()
                    true
                }
                R.id.add_widget -> {
                    selectWidget()
                    true
                }
                R.id.remove_widget -> {
                    val mainLayout = binding.root
                    val childCount: Int = mainLayout.childCount

                    var messageText = "No widgets to remove"

                    if (childCount > 1) {
                        val aView: View = mainLayout.getChildAt(childCount - 1)

                        if (aView is AppWidgetHostView) {
                            removeWidget(aView)
                            messageText = "Removed widget"
                        }
                    }

                    Toast.makeText(app_context, messageText, Toast.LENGTH_SHORT).show()
                    true
                }
                else -> false
            }
        }

        binding.settingsFab.setOnClickListener {
            pum.show()
        }
    }

    private fun selectWidget() {
        val appWidgetId: Int = appWidgetHost.allocateAppWidgetId()
        val pickIntent = Intent(AppWidgetManager.ACTION_APPWIDGET_PICK)
        pickIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
        addEmptyData(pickIntent)
        startActivityForResult(pickIntent, REQUEST_PICK_APPWIDGET)
    }

    private fun addEmptyData(pickIntent: Intent) {
        val customInfo = ArrayList<Parcelable>()
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_INFO, customInfo)
        val customExtras = ArrayList<Parcelable>()
        pickIntent.putParcelableArrayListExtra(AppWidgetManager.EXTRA_CUSTOM_EXTRAS, customExtras)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_PICK_APPWIDGET) {
                configureWidget(data)
            } else if (requestCode == REQUEST_CREATE_APPWIDGET) {
                if (data != null) {
                    createWidget(data)
                } else {
                    throw NullPointerException()
                }
            }
        } else if (resultCode == RESULT_CANCELED && data != null) {
            val appWidgetId = data.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
            if (appWidgetId != -1) {
                appWidgetHost.deleteAppWidgetId(appWidgetId)
            }
        }
    }

    private fun configureWidget(data: Intent?) {
        val extras = data!!.extras
        val appWidgetId = extras!!.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
        val appWidgetInfo: AppWidgetProviderInfo = appWidgetManager.getAppWidgetInfo(appWidgetId)
        if (appWidgetInfo.configure != null) {
            val intent = Intent(AppWidgetManager.ACTION_APPWIDGET_CONFIGURE)
            intent.component = appWidgetInfo.configure
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            startActivityForResult(intent, REQUEST_CREATE_APPWIDGET)
        } else {
            createWidget(data)
        }
    }

    private fun createWidget(data: Intent) {
        val extras = data.extras
        val appWidgetId = extras!!.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, -1)
        val appWidgetInfo: AppWidgetProviderInfo = appWidgetManager.getAppWidgetInfo(appWidgetId)
        val hostView: AppWidgetHostView =
            appWidgetHost.createView(app_context, appWidgetId, appWidgetInfo)
        hostView.setAppWidget(appWidgetId, appWidgetInfo)
        binding.root.addView(hostView)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appWidgetHost.startListening()
    }

    override fun onDestroy() {
        super.onDestroy()
        appWidgetHost.stopListening()
    }

    override fun onStart() {
        super.onStart()
        appWidgetHost.startListening()
    }

    override fun onStop() {
        super.onStop()
        appWidgetHost.stopListening()
    }

    private fun removeWidget(hostView: AppWidgetHostView) {
        appWidgetHost.deleteAppWidgetId(hostView.appWidgetId)
        binding.root.removeView(hostView)
    }
}
