package net.lil.testlauncher

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.coroutineScope
import net.lil.testlauncher.databinding.AppItemBinding

class AppAdapter : RecyclerView.Adapter<AppAdapter.ViewHolder>() {

    val appsList: ArrayList<AppInfo> = ArrayList()

    suspend fun getAppList(c: Context) = coroutineScope {
        val pm: PackageManager = c.packageManager

        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val allApps: List<ResolveInfo> = pm.queryIntentActivities(i, 0)
        for (ri in allApps) {
            appsList.add(
                AppInfo(
                    label = ri.loadLabel(pm),
                    packageName = ri.activityInfo.packageName,
                    icon = ri.activityInfo.loadIcon(pm)
                )
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)

        return ViewHolder(AppItemBinding.inflate(inflater, parent, false), this)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val appLabel = appsList[position].label.toString()
        //val appPackage = appsList[position].packageName.toString()
        val appIcon: Drawable = appsList[position].icon

        holder.bind(appLabel, appIcon)
    }

    override fun getItemCount(): Int = appsList.size

    class ViewHolder(private val binding: AppItemBinding, private val adapter: AppAdapter) :
        RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(appLabel: String, appIcon: Drawable) {
            binding.appItemText.text = appLabel
            binding.appItemImg.setImageDrawable(appIcon)

            itemView.setOnClickListener(this)
            //binding.executePendingBindings()
        }

        override fun onClick(v: View) {
            val pos = adapterPosition
            val context = v.context

            val launchIntent =
                context.packageManager.getLaunchIntentForPackage(adapter.appsList[pos].packageName.toString())
            context.startActivity(launchIntent)
        }

    }
}
