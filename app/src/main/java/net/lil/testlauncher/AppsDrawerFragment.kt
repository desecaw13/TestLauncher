package net.lil.testlauncher

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.coroutines.launch
import net.lil.testlauncher.databinding.FragmentAppsDrawerBinding

class AppsDrawerFragment : Fragment() {

    private lateinit var binding: FragmentAppsDrawerBinding

    private lateinit var sharedPref: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAppsDrawerBinding.inflate(inflater)

        val appAdapter = AppAdapter()

        // viewModelScope
        lifecycleScope.launch { appAdapter.getAppList(requireContext().applicationContext) }

        binding.appDrawerRecycler.adapter = appAdapter

        sharedPref = requireActivity().applicationContext.getSharedPreferences(
            "net.lil.testlauncher_preferences",
            Context.MODE_PRIVATE
        )

        updatePrefs()

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        updatePrefs()
    }

    private fun updatePrefs() {
        (binding.appDrawerRecycler.layoutManager as GridLayoutManager)
            .spanCount = sharedPref.getInt("app_span_count", 4)
    }
}
